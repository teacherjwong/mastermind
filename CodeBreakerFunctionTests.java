
import junit.framework.TestCase;

/**
 * A JUnit test case class.
 * Every method starting with the word "test" will be called when running
 * the test with JUnit.
 */
public class CodeBreakerFunctionTests extends TestCase {
  
  /**
   * A test method.
   * (Replace "X" with a name describing the test.  You may write as
   * many "testSomething" methods in this class as you wish, and each
   * one will be called when running JUnit over this class.)
   */
  public void testGeneratedCodeLength() {
    String s = CodeBreakerFunctions.generateCode(4);
    assertEquals(s.length(), 4);
  }
  
  public void testGeneratedCodeCorrectColors()
  {
    String code = CodeBreakerFunctions.generateCode(100);
    boolean missingColor = false;
    // go through the code character by character
    for(int i = 0; i < code.length(); i++)
    {
      char color = code.charAt(i);
      boolean hasColor = false;
      // check if color exists
      for(int j = 0; j < CodeBreakerFunctions.COLORS.length; j++)
      {
        if(CodeBreakerFunctions.COLORS[j] == color)
        {
          hasColor = true;
        }
      }
      if(!hasColor)
      {
        missingColor = true;
      }
    }
    
    assertEquals(missingColor, false);
  }
<<<<<<< HEAD
  public void testFilterInputLowerCase(){
	  String lowerCase = "rogy";
	  assertEquals(CodeBreakerFunctions.filterInput(lowerCase),"ROGY");
  }
  
  public void testFilterInputSymbol(){
	  String symbolCase = "ai_g";
	  assertEquals(CodeBreakerFunctions.filterInput(symbolCase),null);
=======

  
  
  public void testCheckCodeAllCorrect()
  {
    assertEquals(CodeBreakerFunctions.checkCode("GEWNFJS","GEWNFJS"), true);
  }
  
  public void testCheckCodeCorrectColorWrongPosition()
  {
    assertEquals(CodeBreakerFunctions.checkCode("GEWNFJS","FSJWNEG"), false);
>>>>>>> fc513a80d96fba79027a591aaa974b567c669911
  }
  
  
  public void testCheckCodeAllWrong()
  {
    assertEquals(CodeBreakerFunctions.checkCode("GEWNFJS","PQZXLAT"), false);
  }
  
  
  public void testCheckCodePartiallyCorrect()
  {
    assertEquals(CodeBreakerFunctions.checkCode("GEWNFJS","GEWJDYD"), false);
  }
  
  public void testGenerateFeedbackAllWrongColors() {
	  assertEquals(CodeBreakerFunctions.generateFeedBack("ROGY", "BWPS"), "xxxx");
  }
  public void testGenerateFeedbackAllCorrectColors() {
	  assertEquals(CodeBreakerFunctions.generateFeedBack("ROGY", "ROGY"), "cccc");
  }
  public void testGenerateFeedbackAllContain() {
	  assertEquals(CodeBreakerFunctions.generateFeedBack("ROGY", "GYRO"), "pppp");
  }
  public void testGenerateFeedbackOneCorrectThreeContain() {
	  assertEquals(CodeBreakerFunctions.generateFeedBack("OGRY", "RGYO"), "cppp");
  }
  public void testGenerateFeedbackOneCorrectThreeWrong() {
	  assertEquals(CodeBreakerFunctions.generateFeedBack("ROGY", "RSSS"), "cxxx");
  }
}

