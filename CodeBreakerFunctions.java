import java.util.Scanner;
public class CodeBreakerFunctions
{
  public static final char [] COLORS = {'R', 'O', 'G', 'Y', 'B', 'W', 'P', 'S'};
  /**
   * This generates a code based on the color
   * @param the length of the desired code
   */
  public static String generateCode(int length)
  {
    String code = "";
    // choose the letters for the code
    for(int i = 0; i < length; i++)
    {
      code = code + COLORS[(int)(Math.random()*COLORS.length)];
    }
    return code;
  }
  
  /**
   * This ensure that the input is correct
   * For example, lowercase becomes upper case
   * TODO
   * @param the filtered input
   * @return null if something can not be filtered
   */
 public static String filterInput(String s)
  {
	s = s.toUpperCase();
	int count = 0;
	for (int i = 0; i < s.length();i++) {
		char c = s.charAt(i);
		for (int j = 0; j < COLORS.length;j++) {
			if (c == COLORS[j]) {
				count++;
			}
		}
		if (count == 0) {
			return null;
		}
	}
    return s;
  }
  
  /**
   * Verification of code
   * This checks if the code is correct
   * @return true if correct
   */
  public static boolean checkCode(String guess, String code)
  {
	  for(int i = 0;i<code.length();i++) {
		  if(code.charAt(i)!=guess.charAt(i)) {
			  return false;
		  }
	  }
    return true;
  }
  
  /**
   * Generates the feedback for the user
   * This generates the clues the user need to make the next guess
   */
  public static String generateFeedBack(String code, String s)
  {
	String temp ="";
	String feedback="";
	int correct = 0;
	int contain = 0;
	int notexist = 0;
	
	for(int i=0; i<code.length();i++) {
		if(code.charAt(i)==s.charAt(i)) {
			temp+="c";
		}else if(s.contains(""+code.charAt(i))) {
			temp+="p";
		}else {
			temp+="x";
		}
	}
	for(int j=0; j<temp.length();j++) {
		if(temp.charAt(j)=='c') {
			correct++;
		}else if(temp.charAt(j)=='p') {
			contain++;
		}else if(temp.charAt(j)=='x') {
			notexist++;
		}
	}
	for(int c=0; c<correct;c++) {
		feedback+="c";
	}
	for(int ct=0; ct<contain; ct++) {
		feedback+="p";
	}
	for(int ne=0; ne<notexist; ne++) {
		feedback+="x";
	}
	
    return feedback;
  }
  
  /**
   * The main program
   */
  public static void main(String [] args)
  {
    String theCode = generateCode(4);
    int maxGuess = 10; // the maximum number of times the user can guess
    int currentGuess = 1; // the current guess number of the user
    boolean win = false;
    Scanner sc = new Scanner(System.in);
    
    // our main game loop
    while(!win && currentGuess <= maxGuess)
    {
      // get the user guess
      String theGuess = null;
      while(theGuess == null)
      {
        System.out.println("Please enter a guess: ");
        theGuess = sc.nextLine();
        theGuess = filterInput(theGuess);
      }
      
      // user guess is proper guess
      win = checkCode(theGuess, theCode);
      
      // user guess is wrong, generate feedback
      if(!win)
      {
        String feedback = generateFeedBack(theGuess, theCode);
        System.out.println(feedback);
        currentGuess++;
      }
    }
    
    if(win)
    {
      System.out.println("You win!");
    }
    else
    {
      System.out.println("You lose!");
    }
    
  }
}
